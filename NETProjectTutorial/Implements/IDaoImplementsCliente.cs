﻿using NETProjectTutorial.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NETProjectTutorial.entities;
using System.IO;

namespace NETProjectTutorial.Implements
{
    class IDaoImplementsCliente : IDaoCliente
    {
        //Header cliente
        private BinaryReader brhCliente;
        private BinaryWriter bwhCliente;
        private FileStream fshCliente;
        //Data cliente
        private BinaryReader brdCliente;
        private BinaryWriter bwdCliente;
        private FileStream fsdCliente;

        private const string FILENAME_HEADER = "hcliente.dat";
        private const string FILENAME_DATA = "dcliente.dat";
        private const int SIZE = 390;

        public IDaoImplementsCliente() { }


        #region Abrir y Cerrar flujos
        //Abrir flujo
        private void open()
        {
            try
            {
                                                //Path              //Modo                //Accesos
                fsdCliente = new FileStream(FILENAME_DATA, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                if (File.Exists(FILENAME_HEADER))
                {

                    //Header
                    brhCliente = new BinaryReader(fshCliente);
                    bwhCliente = new BinaryWriter(fshCliente);

                    //Data
                    brdCliente = new BinaryReader(fsdCliente);
                    bwdCliente = new BinaryWriter(fsdCliente);

                    bwhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
                    bwhCliente.Write(0);//n
                    bwhCliente.Write(0);//k
                }
                else
                {
                    fshCliente = new FileStream(FILENAME_HEADER, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    //Header
                    brhCliente = new BinaryReader(fshCliente);
                    bwhCliente = new BinaryWriter(fshCliente);

                    //Data
                    brdCliente = new BinaryReader(fsdCliente);
                    bwdCliente = new BinaryWriter(fsdCliente);
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }

        }

        private void close()
        {
            try
            {
                //Read & Write del header
                if (brhCliente != null)
                {
                    bwhCliente.Close();
                }
                if (bwhCliente != null)
                {
                    brhCliente.Close();
                }

                //Read & Write del data
                if (brdCliente != null)
                {
                    brdCliente.Close();
                }
                if (bwdCliente != null)
                {
                    bwdCliente.Close();
                }

                //FileStream de header & data
                if (fshCliente != null)
                {
                    fshCliente.Close();
                }
                if (fsdCliente != null)
                {
                    fsdCliente.Close();
                }
            }
            catch (IOException e)
            {
                throw new IOException(e.Message);
            }
        }
        #endregion

        public bool delete(Cliente t)
        {
            throw new NotImplementedException();
        }

        public List<Cliente> findAll()
        {
            open();
            List<Cliente> clientes = new List<Cliente>();

            brhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhCliente.ReadInt32();
            for (int i = 0; i < n; i++)
            {
                //Calculamos posición cabecera
                long hpos = 8 + i * 4;
                brhCliente.BaseStream.Seek(hpos, SeekOrigin.Begin);
                int index = brhCliente.ReadInt32();

                //Calculamos posición de los datos
                long dpos = (index - 1) * SIZE;
                brdCliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

                int id = brdCliente.ReadInt32();
                string cedula = brdCliente.ReadString();
                string nombres = brdCliente.ReadString();
                string apellidos = brdCliente.ReadString();
                string telefono = brdCliente.ReadString();
                string correo = brdCliente.ReadString();
                string direccion = brdCliente.ReadString();
                Cliente c = new Cliente(id, cedula, nombres, apellidos, telefono, correo, direccion);
                clientes.Add(c);
            }

            close();
            return clientes;
        }

        public List<Cliente> finByLastname(string lastname)
        {
            throw new NotImplementedException();
        }

        public Cliente findByCedula(string cedula)
        {
            throw new NotImplementedException();
        }

        public Cliente findById(int id)
        {
            throw new NotImplementedException();
        }

        public void save(Cliente t)
        {
            open();
            brhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            int n = brhCliente.ReadInt32();
            int k = brhCliente.ReadInt32();

            long dpos = k * SIZE;  //Calcula la posiciòn en el data
            bwdCliente.BaseStream.Seek(dpos, SeekOrigin.Begin);

            bwdCliente.Write(++k);
            bwdCliente.Write(t.Cedula);
            bwdCliente.Write(t.Nombre);
            bwdCliente.Write(t.Apellido);
            bwdCliente.Write(t.Telefono);
            bwdCliente.Write(t.Correo);
            bwdCliente.Write(t.Direccion);

            bwhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhCliente.Write(++n);
            bwhCliente.Write(k);

            long hpos = 8 + (k - 1) * 4; //Calcula la posiciòn en el header (Temporal, amerita un cambio)
            bwhCliente.BaseStream.Seek(0, SeekOrigin.Begin);
            bwhCliente.Write(k);

            close();
        }

        public int update(Cliente t)
        {
            throw new NotImplementedException();
        }
    }
}
