﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsClientes;
        private BindingSource bsCliente;

        public DataSet DsClientes
        {
            get
            {
                return dsClientes;
            }

            set
            {
                dsClientes = value;
            }
        }

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsClientes;
            bsCliente.DataMember = DsClientes.Tables["Cliente"].TableName;
            dgvCliente.DataSource = bsCliente;
            dgvCliente.AutoGenerateColumns = true;
        }

        private void txtFindClient_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsCliente.Filter = string.Format("Cedula like '*{0}*' or Nombres like '*{0}*' or Apellidos like '*{0}*'", txtFindClient.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.TblClientes = DsClientes.Tables["Cliente"];
            fc.DsClientes = DsClientes;
            fc.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection allRow = dgvCliente.SelectedRows;
            if (allRow.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila para editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow row = allRow[0];
            DataRow dRow = ((DataRowView)row.DataBoundItem).Row;

            FrmCliente fc = new FrmCliente();
            fc.TblClientes = dsClientes.Tables["Cliente"];
            fc.DsClientes = DsClientes;
            fc.DrClientes = dRow;
            fc.ShowDialog();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection allRow = dgvCliente.SelectedRows;
            if (allRow.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila para eliminar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow row = allRow[0];
            DataRow dRow = ((DataRowView)row.DataBoundItem).Row;

            DialogResult res = MessageBox.Show(this, "Realmente desea eliminar el registro?", "Mensaje de confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if(res == DialogResult.Yes)
            {
                DsClientes.Tables["Cliente"].Rows.Remove(dRow);
                MessageBox.Show(this, "Se ha eliminado el registro con éxito!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }
    }
}
