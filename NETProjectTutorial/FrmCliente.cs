﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{

    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drClientes;

        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public DataRow DrClientes
        {
            set
            {
                drClientes = value;
                mskCedula.Text = drClientes["Cédula"].ToString();
                txtName.Text = drClientes["Nombres"].ToString();
                txtLastname.Text = drClientes["Apellidos"].ToString();
                mskPhone.Text = drClientes["Teléfono"].ToString();
                txtEmail.Text = drClientes["Correo"].ToString();
                txtDirect.Text = drClientes["Dirección"].ToString();
            }
        }

        public DataTable TblClientes { get => tblClientes; set => tblClientes = value; }
        public DataSet DsClientes { get => dsClientes; set => dsClientes = value; }

        private void btnAceptar_Click(object sender, EventArgs e)
        {            
            string cedula, names, lastnames, phone, mail, direct;

            cedula = mskCedula.Text;
            names = txtName.Text;
            lastnames = txtLastname.Text;
            phone = mskPhone.Text;
            mail = txtEmail.Text;
            direct = txtDirect.Text;

            if (drClientes != null)
            {
                DataRow dr = TblClientes.NewRow();
                int indice = TblClientes.Rows.IndexOf(drClientes);

                dr["Id"] = drClientes["Id"];
                dr["Cédula"] = cedula;
                dr["Nombres"] = names;
                dr["Apellidos"] = lastnames;
                dr["Teléfono"] = phone;
                dr["Correo"] = mail;
                dr["Dirección"] = direct;

                TblClientes.Rows.RemoveAt(indice);
                TblClientes.Rows.InsertAt(dr, indice);
            }
            else
            {
                TblClientes.Rows.Add(TblClientes.Rows.Count + 1, cedula, names,
                    lastnames, phone, mail, direct);
            }
            Dispose();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Cliente"].TableName;
        }
    }
}
